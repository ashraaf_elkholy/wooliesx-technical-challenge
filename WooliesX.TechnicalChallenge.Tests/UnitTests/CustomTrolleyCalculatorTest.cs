﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WooliesX.TechnicalChallenge.Tests;
using WooliesX.TechnicalChallenge.Web.Models;
using WooliesX.TechnicalChallenge.Web.Services;

namespace WooliesX.TechnicalChallenge.Tests.UnitTests
{
	[TestClass]
	public class CustomTrolleyCalculatorTest
	{
		[TestMethod]
		public async Task TestOneProductTrolley()
		{
			var trolley = new Trolley
			{
				Products = new List<Product>
				{
					new Product { Name = "A" , Price = 3 }
				},
				Specials = new List<SpecialOffer>
				{
					new SpecialOffer
					{
						Quantities =  new List<ProductQuantity>
						{
							new ProductQuantity{ Name = "A" , Quantity = 2 }
						},
						Total = 5
					}
				},
				Quantities = new List<ProductQuantity>
				{
					new ProductQuantity{ Name = "A" , Quantity = 6 }
				}
			};

			var trolleyCalculator = new CustomTrolleyCalculator();

			var actual = await trolleyCalculator.GetLowestTotal(trolley);
			var expected = 15;
			Assert.AreEqual(actual, expected);
		}

		[TestMethod]
		public async Task TestMultipleProductTrolley()
		{
			var trolley = new Trolley
			{
				Products = new List<Product>
				{
					new Product { Name = "A" , Price = 3 },
					new Product { Name = "B" , Price = 4 },
					new Product { Name = "C" , Price = 5 },
					new Product { Name = "D" , Price = 6 }
				},
				Specials = new List<SpecialOffer>
				{
					new SpecialOffer
					{
						Quantities =  new List<ProductQuantity>
						{
							new ProductQuantity{ Name = "A" , Quantity = 2 },
							new ProductQuantity{ Name = "B" , Quantity = 2 },
							new ProductQuantity{ Name = "C" , Quantity = 2 },
							new ProductQuantity{ Name = "D" , Quantity = 2 }
						},
						Total = 25
					}
				},
				Quantities = new List<ProductQuantity>
				{
					new ProductQuantity{ Name = "A" , Quantity = 2 },
					new ProductQuantity{ Name = "B" , Quantity = 2 },
					new ProductQuantity{ Name = "C" , Quantity = 2 },
					new ProductQuantity{ Name = "D" , Quantity = 2 }
				}
			};

			var trolleyCalculator = new CustomTrolleyCalculator();
			var actual = await trolleyCalculator.GetLowestTotal(trolley);
			//expected = 25;
			throw new NotImplementedException();
		}

		[TestMethod]
		public async Task TestAdvancedMultipleProductTrolley()
		{
			var trolley = new Trolley
			{
				Products = new List<Product>
				{
					new Product { Name = "A" , Price = 3 },
					new Product { Name = "B" , Price = 4 },
					new Product { Name = "C" , Price = 5 },
					new Product { Name = "D" , Price = 6 }
				},
				Specials = new List<SpecialOffer>
				{
					new SpecialOffer
					{
						Quantities =  new List<ProductQuantity>
						{
							new ProductQuantity{ Name = "A" , Quantity = 2 },
							new ProductQuantity{ Name = "B" , Quantity = 2 },
							new ProductQuantity{ Name = "C" , Quantity = 2 },
							new ProductQuantity{ Name = "D" , Quantity = 2 }
						},
						Total = 25
					}
				},
				Quantities = new List<ProductQuantity>
				{
					new ProductQuantity{ Name = "A" , Quantity = 2 },
					new ProductQuantity{ Name = "B" , Quantity = 2 },
					new ProductQuantity{ Name = "C" , Quantity = 2 },
					new ProductQuantity{ Name = "D" , Quantity = 3 }
				}
			};

			var trolleyCalculator = new CustomTrolleyCalculator();
			var actual = await trolleyCalculator.GetLowestTotal(trolley);
			//expected = 31;
			throw new NotImplementedException();
		}

	}
}
