using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using WooliesX.TechnicalChallenge.Web.Models;
using WooliesX.TechnicalChallenge.Web.Services;

namespace WooliesX.TechnicalChallenge.UnitTests
{
	[TestClass]
	public class ProductSorterTest
	{
		#region Test Data
		private readonly List<Product> products = new List<Product>
		{	
			new  Product{
				Name =  "Test Product A",
				Price =  99.99M,
				Quantity =  3.0M
			},
			new  Product{
				Name =  "Test Product B",
				Price =  101.99M,
				Quantity =  1.0M
			},
			new  Product{
				Name =  "Test Product F",
				Price =  999999999999.0M,
				Quantity =  1.0M
			},
			new  Product{
				Name =  "Test Product C",
				Price =  10.99M,
				Quantity =  2.0M
			},
			new  Product{
				Name =  "Test Product D",
				Price =  5.0M,
				Quantity =  0.0M
			}
		};

		private readonly List<Customer> shopperHistory = new List<Customer>
		{
			new Customer
			{
				CustomerId = 123,
				Products = new List<Product>{
					new Product
					{
						Name =  "Test Product A",
						Price =  99.99M,
						Quantity =  3
					},
					new Product
					{
						Name = "Test Product B",
						Price = 101.99M,
						Quantity = 1
					},
					new Product
					{
						Name = "Test Product F",
						Price = 999999999999,
						Quantity = 1
					}
				}
			},
			new Customer
			{

				CustomerId= 23,
				Products = new List<Product>
				{
					new Product
					{
						Name =  "Test Product A",
						Price =  99.99M,
						Quantity =  2
					},
					new Product
					{
						Name =  "Test Product B",
						Price =  101.99M,
						Quantity =  3
					},
					new Product
					{
						Name =  "Test Product F",
						Price =  999999999999,
						Quantity =  1
					}
				}
			},
			new Customer
			{
				CustomerId= 23,
				Products =new List<Product>{
					new Product
					{
						Name =  "Test Product C",
						Price =  10.99M,
						Quantity =  2
					},
					new Product
					{
						Name =  "Test Product F",
						Price =  999999999999,
						Quantity =  2
					}
				}
			},
			new Customer
			{
				CustomerId= 23,
				Products = new List<Product>{
					new Product
					{
						Name =  "Test Product A",
						Price =  99.99M,
						Quantity =  1
					},
					new Product
					{
						Name =  "Test Product B",
						Price =  101.99M,
						Quantity =  1
					},
					new Product
					{
						Name =  "Test Product C",
						Price =  10.99M,
						Quantity =  1
					}
				}
			}
		};
		#endregion

		[TestMethod]
		public void TestSortAscending()
		{
			var productSorter = new ProductSorter();
			var sorted = productSorter.SortBy(products, SortOption.Ascending);
			var expectedName = "Test Product A";
			Assert.AreEqual(sorted.First().Name, expectedName);
		}

		[TestMethod]
		public void TestSortDescending()
		{
			var productSorter = new ProductSorter();
			var sorted = productSorter.SortBy(products, SortOption.Descending);
			var expectedName = "Test Product F";
			Assert.AreEqual(sorted.First().Name, expectedName);
		}

		[TestMethod]
		public void TestSortLowPrice()
		{
			var productSorter = new ProductSorter();
			var sorted = productSorter.SortBy(products, SortOption.Low);
			var expectedPrice = 5.0M;
			Assert.AreEqual(sorted.First().Price, expectedPrice);
		}

		[TestMethod]
		public void TestSortHighPrice()
		{
			var productSorter = new ProductSorter();
			var sorted = productSorter.SortBy(products, SortOption.High);
			var expectedPrice = 999999999999.0M;
			Assert.AreEqual(sorted.First().Price, expectedPrice);
		}

		[TestMethod]
		public void TestSortRecommended()
		{
			var productSorter = new ProductSorter();
			var sorted = productSorter.SortByPopularity(shopperHistory, products);
			var expectedMostCommon = "Test Product A";
			var expectedLeastCommon = "Test Product D";
			Assert.AreEqual(sorted.First().Name, expectedMostCommon);
			Assert.AreEqual(sorted.Last().Name, expectedLeastCommon);
		}
	}
}
