﻿using Microsoft.Extensions.Configuration;

namespace WooliesX.TechnicalChallenge.Tests
{
	public static class TestHelper
	{
		public static IConfiguration InitConfiguration()
		{
			var config = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json")
				.Build();
			return config;
		}
	}
}
