﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WooliesX.TechnicalChallenge.Web.Services;
using System.Threading.Tasks;
using System;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Controllers
{
	[Produces("application/json")]
	[Route("api/answers/sort")]
    [ApiController]
	public class ProductController : ControllerBase
    {
		private readonly IConfiguration _config;
		private readonly IProductAdapter _productAdapter;

		public ProductController(IConfiguration config, IProductAdapter productAdapter)
		{ 
			_config = config;
			_productAdapter = productAdapter;
		}

		[HttpGet]
		[ProducesResponseType(typeof(List<Product>), StatusCodes.Status200OK)]
		public async Task<IActionResult> Get(string sortOption)
		{
			var ignoreCase = true;
			if(!Enum.TryParse(sortOption, ignoreCase, out SortOption sortOptionValue))
				sortOptionValue = SortOption.Ascending;
			var result = await _productAdapter.GetSortedProducts(sortOptionValue);
			return Ok(result);
		}
	}
}