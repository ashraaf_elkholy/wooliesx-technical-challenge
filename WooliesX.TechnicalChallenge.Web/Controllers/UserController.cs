﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Controllers
{
	[Produces("application/json")]
	[Route("api/answers/user")]
	[ApiController]
    public class UserController : ControllerBase
    {
		private readonly IConfiguration _config;

		public UserController(IConfiguration config)
		{
			_config = config;
		}

		[HttpGet]
		[ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
		public IActionResult Get()
		{
			var token = _config["AppSettings:Token"];
			var username = _config["AppSettings:Username"];
			var user = new User { Name = username, Token = token };
			return Ok(user);
		}
	}
}