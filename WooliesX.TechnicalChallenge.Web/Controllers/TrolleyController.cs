﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using WooliesX.TechnicalChallenge.Web.Services;

namespace WooliesX.TechnicalChallenge.Web.Controllers
{
	[Produces("application/json")]
	[ApiController]
	public class TrolleyController : ControllerBase
	{
		private readonly IConfiguration _config;
		private readonly ITrolleyCalculator _defaultTrolleyCalculator;

		public TrolleyController(IConfiguration config, ITrolleyCalculator defaultTrolleyCalculator)
		{
			_config = config;
			_defaultTrolleyCalculator = defaultTrolleyCalculator;
		}

		[HttpPost]
		[ProducesResponseType(typeof(decimal), StatusCodes.Status200OK)]
		[Route("api/answers/trolleytotal")]
		public async Task<IActionResult> Default( Trolley trolley)
		{
			var result = await _defaultTrolleyCalculator.GetLowestTotal(trolley);
			return Ok(result);
		}

		[HttpPost]
		[ProducesResponseType(typeof(decimal), StatusCodes.Status200OK)]
		[Route("api/answers/trolleytotal/custom")]
		public async Task<IActionResult> Custom(Trolley trolley)
		{
			var services = HttpContext.RequestServices;
			var _trolleyCalculator = new CustomTrolleyCalculator();

			var result = await _trolleyCalculator.GetLowestTotal(trolley);
			return Ok(result);
		}
	}
}
