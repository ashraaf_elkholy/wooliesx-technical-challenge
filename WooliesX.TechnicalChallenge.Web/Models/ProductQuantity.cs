﻿namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class ProductQuantity
	{
		public string Name { get; set; }
		public int Quantity { get; set; }
	}
}
