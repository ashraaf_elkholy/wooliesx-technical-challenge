﻿using System.Collections.Generic;

namespace WooliesX.TechnicalChallenge.Web.Models
{
	public class Customer
	{
		public int CustomerId { get; set; }
		public List<Product> Products { get; set; }
	}
}
