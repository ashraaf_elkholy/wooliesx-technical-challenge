﻿using System.Collections.Generic;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class Trolley
	{
		public List<Product> Products { get; set; }
		public List<SpecialOffer> Specials { get; set; }
		public List<ProductQuantity> Quantities { get; set; }
	}
}
