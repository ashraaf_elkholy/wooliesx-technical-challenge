﻿namespace WooliesX.TechnicalChallenge.Web.Models
{
	public enum SortOption
	{
		Low,
		High,
		Ascending,
		Descending,
		Recommended
	}
}
