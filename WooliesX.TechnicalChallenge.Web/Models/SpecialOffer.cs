﻿using System.Collections.Generic;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class SpecialOffer
	{
		public List<ProductQuantity> Quantities { get; set; }
		public decimal Total { get; set; }
	}
}
