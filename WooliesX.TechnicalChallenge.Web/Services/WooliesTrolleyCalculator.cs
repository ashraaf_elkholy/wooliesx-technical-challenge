﻿using Microsoft.Extensions.Configuration;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class WooliesTrolleyCalculator : ITrolleyCalculator
	{
		private readonly IConfiguration _config;

		public WooliesTrolleyCalculator(IConfiguration config)
		{
			_config = config;
		}

		public async Task<decimal> GetLowestTotal(Trolley trolley)
		{
			var token = _config["AppSettings:Token"];
			var trolleyCalculatorEndPoint = _config["AppSettings:TrolleyCalculatorEndPoint"];
			var requestUrl = $"{trolleyCalculatorEndPoint}?token={token}";
			
			using (var httpClient = new HttpClient())
			{
				using (var response = await httpClient.PostAsJsonAsync(requestUrl, trolley))
				{
					var total = await response.Content.ReadAsAsync<decimal>();
					return total;
				}
			}
		}
	}
}
