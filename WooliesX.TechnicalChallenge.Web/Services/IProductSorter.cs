﻿using System.Collections.Generic;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public interface IProductSorter
	{
		List<Product> SortBy(List<Product> productList, SortOption sortOption);
		List<Product> SortByPopularity(List<Customer> customerList, List<Product> productList);
	}
}
