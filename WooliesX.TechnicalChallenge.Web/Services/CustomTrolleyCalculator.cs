﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class CustomTrolleyCalculator : ITrolleyCalculator
	{
		public async Task<decimal> GetLowestTotal(Trolley trolley)
		{
			if (IsValid(trolley))
				return await GetProductTotal(trolley);

			throw new ArgumentException();
		}

		// TODO: Get clear requirements for Valid Trolley structure
		private bool IsValid(Trolley trolley)
		{
			if (trolley == null)
				return false;

			if (trolley?.Quantities?.Count == 1 && trolley?.Specials?.FirstOrDefault()?.Quantities?.Count == 1)
				return true;

			// TODO: Get clear requirements for Valid Match : quantities in order versus specials
			// trolley?.Quantities, trolley?.Specials?.FirstOrDefault()?.Quantities
			return false;
		}

		private async Task<decimal> GetProductTotal(Trolley trolley)
		{
			if (trolley.Quantities.Count == 1)
				return await GetSingleProductTotal(trolley);

			return await GetMultipleProductTotal(trolley);
		}

		private async Task<decimal> GetSingleProductTotal(Trolley trolley)
		{
			decimal total = 0;
			foreach (var order in trolley.Quantities)
			{
				var matchingProduct = trolley.Products.SingleOrDefault(p => p.Name.ToLower() == order.Name.ToLower());
				// match product price data
				if (matchingProduct != null)
				{
					var remainder = order.Quantity;
					var relatedSpecial = trolley.Specials.SingleOrDefault(s => s.Quantities.Any(sq => sq.Name.ToLower() == order.Name.ToLower()));
					// match special
					if (relatedSpecial!= null)
					{
						var offerQuantity = relatedSpecial.Quantities.SingleOrDefault(q => q.Name.ToLower() == order.Name.ToLower()).Quantity;
						total += order.Quantity / offerQuantity * relatedSpecial.Total;
						if (order.Quantity % offerQuantity != 0)
						{
							total+= order.Quantity % offerQuantity * matchingProduct.Price;
						}
					}
				}
			}
			return await Task.FromResult(total);
		}

		private async Task<decimal> GetMultipleProductTotal(Trolley trolley)
		{
			throw new NotImplementedException();
		}
	}
}
