﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public interface IProductAdapter
	{
		Task<List<Product>> GetSortedProducts(SortOption sortOption);
	}
}
