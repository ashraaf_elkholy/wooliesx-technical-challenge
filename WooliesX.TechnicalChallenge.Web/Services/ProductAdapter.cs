﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class ProductAdapter : IProductAdapter
	{
		private readonly IConfiguration _config;
		private readonly IProductSorter _productSorter;
		
		public ProductAdapter(IConfiguration config, IProductSorter productSorter)
		{
			_config = config;
			_productSorter = productSorter;
		}

		public async Task<List<Product>> GetSortedProducts(SortOption sortOption)
		{
			if (sortOption == SortOption.Recommended)
			{
				return await GetProductsSortedByPopularity();
			}

			return await GetProductsSortedBy(sortOption);
		}

		private async Task<List<Product>> GetProductsSortedBy(SortOption sortOption)
		{
			var productList = await GetProducts();
			return _productSorter.SortBy(productList, sortOption);
		}

		private async Task<List<Product>> GetProductsSortedByPopularity()
		{
			var customerList = await GetShopperHistory();
			var productList = await GetProducts();
			return _productSorter.SortByPopularity(customerList, productList);
		}

		private async Task<List<Product>> GetProducts()
		{
			var token = _config["AppSettings:Token"];
			var productsEndPoint = _config["AppSettings:ProductsEndPoint"];
			var requestUrl = $"{productsEndPoint}?token={token}";

			var productList = new List<Product>();
			using (var httpClient = new HttpClient())
			{
				using (var response = await httpClient.GetAsync(requestUrl))
				{
					productList = await response.Content.ReadAsAsync<List<Product>>();
				}
			}
			return productList;
		}

		private async Task<List<Customer>> GetShopperHistory()
		{
			var token = _config["AppSettings:Token"];
			var shopperHistoryEndPoint = _config["AppSettings:ShopperHistoryEndPoint"];
			var requestUrl = $"{shopperHistoryEndPoint}?token={token}";

			var customerList = new List<Customer>();
			using (var httpClient = new HttpClient())
			{
				using (var response = await httpClient.GetAsync(requestUrl))
				{
					customerList = await response.Content.ReadAsAsync<List<Customer>>();
				}
			}
			return customerList;
		}
	}
}
