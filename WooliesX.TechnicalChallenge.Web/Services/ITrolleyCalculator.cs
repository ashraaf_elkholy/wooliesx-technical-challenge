﻿using System.Threading.Tasks;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public interface ITrolleyCalculator
	{
		Task<decimal> GetLowestTotal(Trolley trolley);
	}
}
