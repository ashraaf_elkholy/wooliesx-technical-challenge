﻿using System.Collections.Generic;
using System.Linq;
using WooliesX.TechnicalChallenge.Web.Models;

namespace WooliesX.TechnicalChallenge.Web.Services
{
	public class ProductSorter : IProductSorter
	{

		public List<Product> SortBy(List<Product> productList, SortOption sortOption)
		{
			switch (sortOption)
			{
				case SortOption.Ascending:
					return productList.OrderBy(p => p.Name).ToList();
				case SortOption.Descending:
					return productList.OrderByDescending(p => p.Name).ToList();
				case SortOption.Low:
					return productList.OrderBy(p => p.Price).ToList();
				default:
					return productList.OrderByDescending(p => p.Price).ToList();
			}
		}

		public List<Product> SortByPopularity(List<Customer> customerList, List<Product> productList)
		{
			var purchasedProducts = customerList.SelectMany(c => c.Products).GroupBy(p => p.Name)
					.OrderByDescending(g => g.Count())
					.Select(gp => gp.First())
					.ToList();
			var unpurchasedProducts = productList.Where(p => !purchasedProducts.Any(pp => pp.Name == p.Name)).OrderBy(o => o.Name);
			var sortedProducts = purchasedProducts.Union(unpurchasedProducts).ToList();
			return sortedProducts;
		}
	}
}
